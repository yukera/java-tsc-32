package com.tsc.jarinchekhina.tm.component;

import com.tsc.jarinchekhina.tm.comparator.ComparatorFileByLastModified;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;

public class FileScanner implements Runnable {

    @NotNull
    private Predicate<File> predicateIsFile() {
        return File::isFile;
    }

    @NotNull
    private Predicate<String> predicateIsFileCommand(@NotNull final String fileName) {
        return command -> command.equals(fileName);
    }

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Collection<String> commands = new ArrayList<>();

    public FileScanner(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @SneakyThrows
    public void init() {
        if (DataUtil.isEmpty(commands)) commands.addAll(bootstrap.getCommandService().getCommandNames());
        @NotNull final Long delay = bootstrap.getPropertyService().getBackupInterval();
        executorService.scheduleWithFixedDelay(this,0, delay, TimeUnit.SECONDS);
    }

    public void run() {
        @NotNull final File file = new File(bootstrap.getPropertyService().getFilescannerPath());
        @Nullable final List<File> files = Arrays.asList(file.listFiles());
        files.stream()
                .filter(predicateIsFile())
                .sorted(ComparatorFileByLastModified.getInstance())
                .forEach(item -> commands.stream()
                        .filter(predicateIsFileCommand(item.getName()))
                        .forEach(cmd -> {
                            bootstrap.parseCommand(cmd);
                            item.delete();
                        }));
    }

}
